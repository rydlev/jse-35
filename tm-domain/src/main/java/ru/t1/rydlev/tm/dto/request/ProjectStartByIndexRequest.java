package ru.t1.rydlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectStartByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
