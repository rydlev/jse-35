package ru.t1.rydlev.tm.api.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import lombok.SneakyThrows;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.endpoint.IConnectionProvider;
import ru.t1.rydlev.tm.api.endpoint.IEndpoint;

import javax.jws.WebMethod;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public interface IEndpointClient extends IEndpoint {

    @NotNull
    String HOST = "localhost";

    @NotNull
    String PORT = "8080";

    @NotNull
    String SPACE = "http://endpoint.tm.rydlev.t1.ru/";

    @NotNull
    JacksonJaxbJsonProvider JSON_PROVIDER = new JacksonJaxbJsonProvider();

    @NotNull
    List<?> PROVIDERS = Collections.singletonList(JSON_PROVIDER);

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String name, @NotNull final String space,
            @NotNull final String part, @NotNull final Class<T> clazz
    ) {
        @NotNull final String host = HOST;
        @NotNull final String port = PORT;
        return newInstance(host, port, name, space, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final IConnectionProvider connectionProvider,
            @NotNull final String name, @NotNull final String space,
            @NotNull final String part, @NotNull final Class<T> clazz
    ) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return newInstance(host, port, name, space, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host, @NotNull final String port,
            @NotNull final String name, @NotNull final String space,
            @NotNull final String part, @NotNull final Class<T> clazz
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(space, part);
        return Service.create(url, qName).getPort(clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host,
            @NotNull final String port,
            @NotNull final Class<T> clazz
    ) {
        @NotNull final String baseUrl = "http://" + host + ":" + port + "/";
        return JAXRSClientFactory.create(baseUrl, clazz, PROVIDERS);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(@NotNull final Class<T> clazz) {
        return newInstance(HOST, PORT, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final IConnectionProvider connectionProvider,
            @NotNull final Class<T> clazz
    ) {
        return newInstance(connectionProvider.getHost(), connectionProvider.getPort(), clazz);
    }

}
