package ru.t1.rydlev.tm.api.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IConnectionProvider;
import ru.t1.rydlev.tm.api.endpoint.IDomainEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDomainEndpointClient extends IEndpointClient, IDomainEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstanceSoap() {
        return newInstanceSoap(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstanceSoap(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpointClient.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstanceSoap(@NotNull final String host, @NotNull final String port) {
        return IEndpointClient.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstanceRest() {
        return IEndpointClient.newInstance(IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstanceRest(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpointClient.newInstance(connectionProvider, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstanceRest(@NotNull final String host, @NotNull final String port) {
        return IEndpointClient.newInstance(host, port, IDomainEndpoint.class);
    }

    static void main(String[] args) {
        System.out.println(IDomainEndpointClient.newInstanceRest().ping().getSuccess());
    }

}
