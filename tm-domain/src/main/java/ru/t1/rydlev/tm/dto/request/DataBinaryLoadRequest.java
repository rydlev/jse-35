package ru.t1.rydlev.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBinaryLoadRequest extends AbstractUserRequest {

    public DataBinaryLoadRequest(@Nullable final String token) {
        super(token);
    }

}
