package ru.t1.rydlev.tm.repository;

import com.hazelcast.core.HazelcastInstance;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    public TaskRepository(@NotNull final HazelcastInstance instance) {
        super(instance);
    }

    @NotNull
    private Predicate<Task> filterByProjectId(@NotNull final String projectId) {
        return m -> projectId.equals(m.getProjectId());
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .filter(filterByProjectId(projectId))
                .collect(Collectors.toList());
    }

}
