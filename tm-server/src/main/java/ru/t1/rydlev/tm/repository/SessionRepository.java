package ru.t1.rydlev.tm.repository;

import com.hazelcast.core.HazelcastInstance;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.repository.ISessionRepository;
import ru.t1.rydlev.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository() {
    }

    public SessionRepository(@NotNull final HazelcastInstance instance) {
        super(instance);
    }

}

