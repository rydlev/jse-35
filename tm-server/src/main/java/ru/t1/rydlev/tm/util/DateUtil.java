package ru.t1.rydlev.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    SimpleDateFormat TO_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    @NotNull
    static String formatDate(Date date) {
        return TO_FORMAT.format(date);
    }

}
