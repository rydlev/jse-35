package ru.t1.rydlev.tm.endpoint;

import io.swagger.annotations.ApiParam;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.IUserEndpoint;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.Result;
import ru.t1.rydlev.tm.dto.response.*;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.model.Session;
import ru.t1.rydlev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@NoArgsConstructor
@Path("/api/UserEndpoint")
@WebService(endpointInterface = "ru.t1.rydlev.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result ping() {
        return new Result();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/changeUserPassword")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserChangePasswordResponse changeUserPassword(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getNewPassword();
        getServiceLocator().getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/lockUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserLockResponse lockUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/registryUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserRegistryResponse registryUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final User user = getServiceLocator().getAuthService().registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/removeUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserRemoveResponse removeUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/unlockUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserUnlockResponse unlockUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/updateUserProfile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserUpdateProfileResponse updateUserProfile(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        getServiceLocator().getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse();
    }

}
