package ru.t1.rydlev.tm.api.component;

import org.jetbrains.annotations.Nullable;

public interface IBootstrap {

    void run();

}
