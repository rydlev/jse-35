package ru.t1.rydlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rydlev.tm.api.repository.ISessionRepository;
import ru.t1.rydlev.tm.marker.UnitCategory;
import ru.t1.rydlev.tm.model.Session;

import java.util.Collections;

import static ru.t1.rydlev.tm.constant.SessionTestData.*;
import static ru.t1.rydlev.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.rydlev.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @Before
    public void before() {
        repository.add(USER_SESSION1);
        repository.add(USER_SESSION2);
    }

    @After
    public void after() {
        repository.removeAll(SESSION_LIST);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_SESSION1));
        @Nullable final Session session = repository.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_SESSION_LIST));
        for (final Session session : ADMIN_SESSION_LIST)
            Assert.assertEquals(session, repository.findOneById(session.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertNull(repository.add(null, ADMIN_SESSION1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_SESSION1));
        @Nullable final Session session = repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void set() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION_LIST);
        emptyRepository.set(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION_LIST);
        Assert.assertEquals(USER_SESSION_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_SESSION_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_SESSION1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_TEST.getId(), USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIndex() {
        final int index = repository.findAll().indexOf(USER_SESSION1);
        @Nullable final Session session = repository.findOneByIndex(index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = repository.findAll().indexOf(USER_SESSION1);
        @Nullable final Session session = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void clear() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION1);
        emptyRepository.add(USER_SESSION2);
        emptyRepository.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyRepository.getSize(USER_TEST.getId()));
    }

    @Test
    public void remove() {
        Assert.assertNull(repository.remove(null));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = repository.remove(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertNull(repository.remove(ADMIN_TEST.getId(), null));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        Assert.assertNull(repository.remove(null, createdSession));
        @Nullable final Session removedSession = repository.remove(ADMIN_TEST.getId(), createdSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_SESSION_ID));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = repository.removeById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), USER_SESSION1.getId()));
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        Assert.assertNull(repository.removeById(null, createdSession.getId()));
        @Nullable final Session removedSession = repository.removeById(ADMIN_TEST.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndex() {
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        final int index = repository.findAll().indexOf(createdSession);
        @Nullable final Session removedSession = repository.removeByIndex(index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndexByUserId() {
        @Nullable final Session createdSession = repository.add(ADMIN_SESSION1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(createdSession);
        @Nullable final Session removedSession = repository.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_SESSION1);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize(ADMIN_TEST.getId()));
        emptyRepository.add(ADMIN_SESSION1);
        emptyRepository.add(USER_SESSION1);
        Assert.assertEquals(1, emptyRepository.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(SESSION_LIST);
        emptyRepository.removeAll(SESSION_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

}
