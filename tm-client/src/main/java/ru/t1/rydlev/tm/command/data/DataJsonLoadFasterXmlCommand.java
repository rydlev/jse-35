package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataJsonLoadFasterXmlRequest;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA LOAD JSON");
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(getToken());
        getDomainEndpointClient().loadDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from json file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-json";
    }

}
