package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.ProjectShowByIndexRequest;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Project project = getProjectEndpoint().showProjectByIndex(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

}
