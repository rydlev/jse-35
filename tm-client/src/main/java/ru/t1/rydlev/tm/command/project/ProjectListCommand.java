package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.ProjectListRequest;
import ru.t1.rydlev.tm.enumerated.Sort;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.util.DateUtil;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<Project> projects = getProjectEndpoint().listProject(request).getProjects();
        if (projects == null) return;
        int index = 1;
        for (final Project project : projects) {
            final String id = project.getId();
            final String name = project.getName();
            final String description = project.getDescription();
            final String status = Status.toName(project.getStatus());
            final String created = DateUtil.formatDate(project.getCreated());
            System.out.printf("%s. %s : %s : %s : %s; id: %s \n", index, name, status, created, description, id);
            index++;
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

}
