package ru.t1.rydlev.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;


@UtilityClass
public class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "USER_TEST_LOGIN";

    @NotNull
    public final static String USER_TEST_PASSWORD = "USER_TEST_PASSWORD";

    @NotNull
    public final static String USER_TEST_EMAIL = "USER_TEST_EMAIL";

    @NotNull
    public final static String ADMIN_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_PASSWORD = "admin";

    @NotNull
    public final static String USER_TEST_FIRST_NAME = "USER_TEST_FIRST_NAME";

    @NotNull
    public final static String USER_TEST_LAST_NAME = "USER_TEST_LAST_NAME";

    @NotNull
    public final static String USER_TEST_MIDDLE_NAME = "USER_TEST_MIDDLE_NAME";

}
