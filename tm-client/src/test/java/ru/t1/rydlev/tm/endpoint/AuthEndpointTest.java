package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rydlev.tm.api.client.IAuthEndpointClient;
import ru.t1.rydlev.tm.api.client.IUserEndpointClient;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IUserEndpoint;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.marker.IntegrationCategory;
import ru.t1.rydlev.tm.model.User;
import ru.t1.rydlev.tm.service.PropertyService;

import static ru.t1.rydlev.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT_CLIENT = IAuthEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT_CLIENT = IUserEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = AUTH_ENDPOINT_CLIENT.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        USER_ENDPOINT_CLIENT.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        USER_ENDPOINT_CLIENT.removeUser(request);
    }

    private String getUserToken() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        return AUTH_ENDPOINT_CLIENT.loginUser(request).getToken();
    }

    @Test
    public void loginUser() {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        Assert.assertNotNull(AUTH_ENDPOINT_CLIENT.viewUserProfile(viewRequest).getUser());
    }

    @Test
    public void logoutUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        AUTH_ENDPOINT_CLIENT.logoutUser(request);
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT_CLIENT.viewUserProfile(viewRequest));
    }

    @Test
    public void viewProfileUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(token);
        @Nullable final User user = AUTH_ENDPOINT_CLIENT.viewUserProfile(request).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST_LOGIN, user.getLogin());
    }

}
