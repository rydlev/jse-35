package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rydlev.tm.api.client.IAuthEndpointClient;
import ru.t1.rydlev.tm.api.client.IProjectEndpointClient;
import ru.t1.rydlev.tm.api.client.ITaskEndpointClient;
import ru.t1.rydlev.tm.api.client.IUserEndpointClient;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IProjectEndpoint;
import ru.t1.rydlev.tm.api.endpoint.ITaskEndpoint;
import ru.t1.rydlev.tm.api.endpoint.IUserEndpoint;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.dto.request.*;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.marker.IntegrationCategory;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.service.PropertyService;

import java.util.List;

import static ru.t1.rydlev.tm.constant.ProjectTestData.USER_PROJECT1_DESCRIPTION;
import static ru.t1.rydlev.tm.constant.ProjectTestData.USER_PROJECT1_NAME;
import static ru.t1.rydlev.tm.constant.TaskTestData.*;
import static ru.t1.rydlev.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT_CLIENT = IAuthEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT_CLIENT = IUserEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT_CLIENT = IProjectEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @NotNull
    private static final ITaskEndpoint TASK_ENDPOINT_CLIENT = ITaskEndpointClient.newInstanceSoap(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    @Nullable
    private String taskId1;

    private int taskIndex1;

    @Nullable
    private String taskId2;

    private int taskIndex2;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = AUTH_ENDPOINT_CLIENT.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        request.setEmail(USER_TEST_EMAIL);
        USER_ENDPOINT_CLIENT.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = AUTH_ENDPOINT_CLIENT.loginUser(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        USER_ENDPOINT_CLIENT.removeUser(request);
    }

    @Before
    public void before() {
        taskId1 = createTestTask(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);
        taskIndex1 = 0;
        taskId2 = createTestTask(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);
        taskIndex2 = 1;
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
    }

    @After
    public void after() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.clearTask(request));
    }

    @NotNull
    private String createTestTask(final String name, final String description) {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(name);
        taskCreateRequest.setDescription(description);
        return TASK_ENDPOINT_CLIENT.createTask(taskCreateRequest).getTask().getId();
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return PROJECT_ENDPOINT_CLIENT.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private Task findTestTaskById(final String id) {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(id);
        return TASK_ENDPOINT_CLIENT.showTaskById(request).getTask();
    }

    @Test
    public void changeStatusByIdTask() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIdRequest taskCreateRequestNullToken = new TaskChangeStatusByIdRequest(null);
        taskCreateRequestNullToken.setId(taskId1);
        taskCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> TASK_ENDPOINT_CLIENT.changeTaskStatusById(taskCreateRequestNullToken));
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setId(taskId1);
        request.setStatus(status);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.changeTaskStatusById(request));
        @Nullable final Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void changeStatusByIndexTask() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        request.setStatus(status);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.changeTaskStatusByIndex(request));
        @Nullable final Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearTask() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.clearTask(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNull(task);
        task = findTestTaskById(taskId2);
        Assert.assertNull(task);
    }

    @Test
    public void createTask() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(USER_TASK3_NAME);
        taskCreateRequest.setDescription(USER_TASK3_DESCRIPTION);
        @Nullable Task task = TASK_ENDPOINT_CLIENT.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3_NAME, task.getName());
        Assert.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    @Test
    public void completeByIdTask() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.completeTaskById(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void completeByIndexTask() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.completeTaskByIndex(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void listTask() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        @Nullable final List<Task> tasks = TASK_ENDPOINT_CLIENT.listTask(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (Task task : tasks) {
            Assert.assertNotNull(findTestTaskById(task.getId()));
        }
    }

    @Test
    public void removeByIdTask() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setId(taskId2);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.removeTaskById(request));
        Assert.assertNull(findTestTaskById(taskId2));
    }

    @Test
    public void removeByIndexTask() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken);
        request.setIndex(taskIndex2);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.removeTaskByIndex(request));
        Assert.assertNull(findTestTaskById(taskId2));
    }

    @Test
    public void showByIdTask() {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(taskId1);
        @Nullable final Task task = TASK_ENDPOINT_CLIENT.showTaskById(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId1, task.getId());
    }

    @Test
    public void showByIndexTask() {
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        @Nullable final Task task = TASK_ENDPOINT_CLIENT.showTaskByIndex(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId1, task.getId());
    }

    @Test
    public void startByIdTask() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.startTaskById(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void startByIndexTask() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.startTaskByIndex(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void updateByIdTask() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setId(taskId1);
        request.setName(USER_TASK3_NAME);
        request.setDescription(USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.updateTaskById(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3_NAME, task.getName());
        Assert.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    @Test
    public void updateByIndexTask() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        request.setName(USER_TASK3_NAME);
        request.setDescription(USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.updateTaskByIndex(request));
        @Nullable Task task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3_NAME, task.getName());
        Assert.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    private void bindTaskProject(final String projectId, final String taskId) {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        TASK_ENDPOINT_CLIENT.bindTaskToProject(request);
    }

    @Test
    public void bindToProjectTask() {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.bindTaskToProject(request));
        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(projectId1);
        @Nullable final List<Task> tasks = TASK_ENDPOINT_CLIENT.showTaskByProjectId(requestShow).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assert.assertEquals(taskId1, tasks.get(0).getId());
    }

    @Test
    public void showByProjectIdTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(userToken);
        request.setProjectId(projectId1);
        @Nullable final List<Task> tasks = TASK_ENDPOINT_CLIENT.showTaskByProjectId(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (Task task : tasks) {
            Assert.assertEquals(projectId1, task.getProjectId());
        }
    }

    @Test
    public void unbindFromProjectTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assert.assertNotNull(TASK_ENDPOINT_CLIENT.unbindTaskFromProject(request));
        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(projectId1);
        @Nullable final List<Task> tasks = TASK_ENDPOINT_CLIENT.showTaskByProjectId(requestShow).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assert.assertEquals(taskId2, tasks.get(0).getId());
    }

}
