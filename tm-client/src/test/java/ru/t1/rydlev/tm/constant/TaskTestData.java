package ru.t1.rydlev.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String USER_TASK1_NAME = "User Test Task 1";

    @NotNull
    public final static String USER_TASK1_DESCRIPTION = "User Test Task Description 1";

    @NotNull
    public final static String USER_TASK2_NAME = "User Test Task 2";

    @NotNull
    public final static String USER_TASK2_DESCRIPTION = "User Test Task Description 2";

    @NotNull
    public final static String USER_TASK3_NAME = "User Test Task 3";

    @NotNull
    public final static String USER_TASK3_DESCRIPTION = "User Test Task Description 3";

}
